.DEFAULT_GOAL := help

## DEV ##
TAG_MYSQL 		= mysql

## RESULT_VARS ##
DOCKER_NETWORK 		= base_network
PROJECT_NAME 		= services
IMAGE_MYSQL			= ${PROJECT_NAME}:${TAG_MYSQL}

build: ## build IMAGE, use me with: make build
	docker build -f docker/mysql/Dockerfile -t ${IMAGE_MYSQL} docker/mysql/

up: ## up docker containers, use me with: make up
	@make verify_network &> /dev/null
	DOCKER_NETWORK=$(DOCKER_NETWORK) \
	docker-compose -p $(PROJECT_NAME) up -d

stop:
	docker-compose -p $(PROJECT_NAME) stop

down: ## Stops and removes the docker containers, use me with: make down
	docker-compose -p $(PROJECT_NAME) down

restart: ## Restart all containers, use me with: make restart
	docker-compose -p $(PROJECT_NAME) restart
	docker-compose -p $(PROJECT_NAME) ps

status: ## Show containers status, use me with: make status
	docker-compose -p $(PROJECT_NAME) ps

ssh: ## Connect to conainer for ssh protocol, use me with: make ssh c=<container_name>
	docker exec -it $(c) bash

up-db:
	docker run --rm -d \
		--name $(PROJECT_NAME)_mysql \
		-p 3306:3306 \
		-e "MYSQL_ROOT_PASSWORD=123456" \
		-v $$PWD/data/mysql:/var/lib/mysql \
		-v $$PWD/docker/mysql/initdb:/docker-entrypoint-initdb.d \
		--net neo_network \
		$(IMAGE_MYSQL)
	#IMAGE_MYSQL=$(IMAGE_MYSQL) \
	#CONTAINER_NAME=$(PROJECT_NAME)_mysql \
	#DOCKER_NETWORK=$(DOCKER_NETWORK) \
	#docker-compose -f docker-compose.db.yml -p $(PROJECT_NAME)_mysql up -d

verify_network:
	@if [ -z $$(docker network ls | grep $(DOCKER_NETWORK) | awk '{print $$2}') ]; then\
		(docker network create $(DOCKER_NETWORK));\
	fi

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'
